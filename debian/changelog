tcpick (0.2.1-11) unstable; urgency=medium

  * Bump to Standards-Version 4.6.2 (no changes required).

 -- Marcos Fouces <marcos@debian.org>  Fri, 03 Mar 2023 00:33:43 +0100

tcpick (0.2.1-10) unstable; urgency=medium

  * Update uploader email to @debian.org.
  * Bump to Standards-Version 4.5.1 (no changes required).
  * Add *.asc file with upstream signature in package git repo.
  * Update and improve upstream metadata.

 -- Marcos Fouces <marcos@debian.org>  Sun, 21 Feb 2021 00:23:13 +0100

tcpick (0.2.1-9) unstable; urgency=medium

  * d/control
    - Bump debhelper compatibility level to 13
    - Switch Build-Depends from debhelper to debhelper-compat, dropping
      d/compat as it is obsolete.
    - Bump to Standards-Version 4.5.0 (no changes required).
    - Add Rules-Requires-Root: no in source stanza.
  * d/patches
    - Add fix-gcc-10.patch. (Closes: #957860).
    - Refresh all patches.

 -- Marcos Fouces <marcos.fouces@gmail.com>  Fri, 19 Apr 2019 23:43:53 +0200

tcpick (0.2.1-8) unstable; urgency=medium

  [ Raphaël Hertzog ]
  * Update team maintainer address to
    Debian Security Tools <team+pkg-security@tracker.debian.org>
  * Update Vcs-Git and Vcs-Browser for the move to salsa.debian.org

  [ Marcos Fouces ]
  * Update uploader email.
  * Bump to dh compat level 11.
  * Add upstream metadata.
  * Bump to Standards-revision 4.2.1 (no changes needed).
  * Add pgp upstream signature check.
  * Add copyright year.

 -- Marcos Fouces <marcos.fouces@gmail.com>  Sun, 04 Nov 2018 13:48:34 +0100

tcpick (0.2.1-7) unstable; urgency=medium

  * Fix watch file
  * Assign maintenance to pkg-security team (Closes:#847511)
  * Bump to dehelper compat level 10
  * Rename docs to tcpick.docs
  * Add set-timestamp-pcap-header-structure.patch from Ubuntu
  * delete unneded file (patches/CHANGES)
  * Bump to standards-revision 3.9.8
  * add fix-spelling-errors.patch
  * Add more docs to package

 -- Marcos Fouces <mfouces@yahoo.es>  Sat, 10 Dec 2016 09:46:02 +0100

tcpick (0.2.1-6.1) unstable; urgency=high

  * Non-maintainer upload (but really a QA upload as maintainer is likely
    MIA).
  * Change source format to 3.0 (quilt) so that we can properly handle
    upstream patches.
  * Split diff.gz into all the historic patches that have been applied.
  * Switch debhelper compat level to 9 and uses short rules file.
  * Use dh-autoreconf instead of manual copying of config.{sub,guess}.
  * Update Standards-Version to 3.9.6.
  * Fix build failure with GCC 5 (as well as multiple other problems
    reported via GCC warnings). Closes: #778141

 -- Raphaël Hertzog <hertzog@debian.org>  Mon, 13 Jul 2015 10:19:37 +0200

tcpick (0.2.1-6) unstable; urgency=low

  * New maintainer (Closes:#430030).

 -- William Vera <billy@billy.com.mx>  Mon, 08 Jun 2009 05:32:25 -0500

tcpick (0.2.1-5) unstable; urgency=low

  * QA upload.
  * Move Homepage from package description to source stanza.
  * Update watch file. (Closes: #449827).
    + Thanks to Raphael Geissert for the fix!
  * Remove useless maintainer scripts.
  * Add appropriate Copyright holders to debian/copyright.
  * Bump debhelper build-dep and compat to 5.
  * Bump Standards Version to 3.8.1.

 -- Barry deFreese <bdefreese@debian.org>  Wed, 06 May 2009 17:53:22 -0400

tcpick (0.2.1-4) unstable; urgency=low

  * QA upload.
  * Set maintainer to QA Group; Orphaned: #430030
  * Silence lintian about clean target in debian/rules
  * Update FSF Address in debian/copyright
  * Conforms with latest Standards Version 3.7.2

 -- Michael Ablassmeier <abi@debian.org>  Fri, 20 Jul 2007 09:48:22 +0200

tcpick (0.2.1-3) unstable; urgency=high

  * src/write.c: temporary patch to fix CVE-2006-0048 (Closes: Bug#360571)
    As upstream is not responsive, I have written this one-line patch.
    With the option -yP, tcpick shows data contained in the captured packets.
    For some packets, tcpick computes a negative buffer length, which is used
    in a while (buffer length) {} loop to display the packet content. When the
    buffer length is negative, the loop never ends, and tcpick segfaults after
    a while.
    This patch tests if the computed buffer length is negative before using
    it, and set it to 0 in this case.

 -- Cédric Delfosse <cedric@debian.org>  Fri, 14 Apr 2006 20:59:07 +0200

tcpick (0.2.1-2) unstable; urgency=low

  * Patch to make it run on ppc, thanks to Alan Curry (Closes: Bug#327327)
  * Patch to fix segfault on 64 bit architecture, thanks to Dann Frazier
    (Closes: Bug#326927)
  * Patch to fix a double free that make tcpick CPU loops (Closes: Bug#319864)

 -- Cédric Delfosse <cedric@debian.org>  Tue,  4 Oct 2005 22:35:16 +0200

tcpick (0.2.1-1) unstable; urgency=low

  * New upstream release

 -- Cédric Delfosse <cedric@debian.org>  Fri, 21 Jan 2005 22:25:37 +0100

tcpick (0.1.24-1) unstable; urgency=low

  * New upstream release

 -- Cédric Delfosse <cedric@debian.org>  Thu, 16 Sep 2004 21:07:22 +0200

tcpick (0.1.23-2) unstable; urgency=low

  * src/args.c: right command to invoke the man page. (Closes: Bug#265067)

 -- Cédric Delfosse <cedric@debian.org>  Wed, 11 Aug 2004 19:32:53 +0200

tcpick (0.1.23-1) unstable; urgency=low

  * Initial Release.

 -- Cédric Delfosse <cedric@debian.org>  Sun, 11 Jul 2004 00:23:46 +0200
